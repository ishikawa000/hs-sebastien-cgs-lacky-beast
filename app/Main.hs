{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE TypeOperators #-}

module Main where

import Control.Monad (forM_, (>=>))
import Data.Aeson (ToJSON, FromJSON)
import Data.Typeable (Proxy(..))
import GHC.Generics (Generic)
import Safe (headMay, readMay)
import Servant ((:>), Get, JSON,  serve, (:<|>)(..), Post, ReqBody, Server)
import System.Environment (getArgs)
import Test.QuickCheck (Arbitrary(..))
import qualified Network.Wai.Handler.Warp as W

type Slot = String

data CGSRequestArgs = CGSRequestArgs
  { utterance :: String -- ^ ユーザ発話
  , intent :: String -- ^ Intent名
  , slots :: [Slot] -- ^ slotのentity
  } deriving (Show, Read, Generic)

instance FromJSON CGSRequestArgs
instance ToJSON CGSRequestArgs

instance Arbitrary CGSRequestArgs where
  arbitrary = CGSRequestArgs <$> arbitrary <*> arbitrary <*> arbitrary

data CGSRequest = CGSRequest
  { botId :: Int -- ^ 内部的に払い出されたID
  , userId :: Int -- ^ ユーザ識別ID
  , lang :: String
  , args :: CGSRequestArgs
  } deriving (Show, Read, Generic)

instance FromJSON CGSRequest
instance ToJSON CGSRequest

instance Arbitrary CGSRequest where
  arbitrary = CGSRequest <$> arbitrary
                         <*> arbitrary
                         <*> arbitrary
                         <*> arbitrary


data CGSResponseParams = CGSResponseParams
  { status :: Bool
  , message :: String -- ^ CGS側が返すシステム発話
  } deriving (Show, Read, Generic)

instance ToJSON CGSResponseParams

instance Arbitrary CGSResponseParams where
  arbitrary = CGSResponseParams <$> arbitrary <*> arbitrary

data CGSResponse = CGSResponse
  { errorCode :: String
  , status :: Bool
  , botId :: Int -- ^ 内部的に払い出されたID
  , userId :: Int -- ^ ユーザ識別ID
  , params :: CGSResponseParams
  } deriving (Show, Read, Generic)

instance ToJSON CGSResponse

instance Arbitrary CGSResponse where
  arbitrary = CGSResponse <$> arbitrary
                          <*> arbitrary
                          <*> arbitrary
                          <*> arbitrary
                          <*> arbitrary


type BossCGS = "lackyBeast" :> Get '[JSON] CGSResponse
          :<|> "lackyBeast" :> ReqBody '[JSON] :> Post '[JSON] CGSResponse

boss :: CGSResponse
boss = CGSResponse
  { errorCode = ""
  , status = True
  , botId = (-1) -- ??
  , userId = (-1) -- ??
  , params = CGSResponseParams True "ﾎﾞｸ ﾊ ﾗｯｷｰﾋﾞｰｽﾄ ﾀﾞﾖ"
  }


main :: IO ()
main = do
  maybePort <- (headMay >=> readMay) <$> getArgs
  forM_ maybePort $ \port ->
    W.run port $ serve bossCGS server
  where
    bossCGS :: Proxy BossCGS
    bossCGS = Proxy
    server :: Server BossCGS
    server = :<|> 
